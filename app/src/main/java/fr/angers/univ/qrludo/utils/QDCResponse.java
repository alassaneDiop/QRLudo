package fr.angers.univ.qrludo.utils;


public interface QDCResponse {
    void processFinish(Boolean output);
}
