# Règles de confidentialité de QR Ludo

## Appareil photo
Scan des QR Codes (aucune photos ou vidéos ne sont prises et aucune données ne sont envoyés via internet).
## Accès à Internet
Téléchargement des fichiers sons stockés dans les QR codes. Aucune donnée n’est envoyé via internet.
## Accès au système de fichiers
Sauvegarde des fichiers sons téléchargés sur le disque interne du téléphone. Aucune donnée n’est importée ou envoyée via internet.
## Données personnelles
Aucune donnée personnelle n’est collectée par QR Ludo.
